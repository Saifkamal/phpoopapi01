<?php
class Database{
    //DB Params
    private $host   = 'localhost';
    private $dbName = 'myblog';
    private $user   = 'sk123';
    private $pass   = 'sk!@#789';
    private $conn;

    //DB Connect
    public function connect(){
        $this->conn = null;
        try {
            $this->conn = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName,$this->user,$this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection Error'.$e->getMessage();
        }
        return $this->conn;
    }
}