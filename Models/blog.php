<?php
class Blog{
    //DB Stuff
    private $conn;
    private $table = "articles";

    //Blog Properties
    public $id;
    public $note_id;
    public $d_title;
    public $d_content;
    public $d_resources;
    public $d_topics;
    public $d_type;
    public $create_time;
    public $d_abstract;
    public $d_keywords;


    //Constructor with DB
    public function __construct($db){
        $this->conn = $db;
    }

    //Get Blog Posts
    public function read(){
        //Create Query
        $query = "
            SELECT
            a.note_id,
            a.id,
            a.d_abstract,
            a.d_content,
            a.d_keywords,
            a.d_resources,
            a.d_title,
            a.d_topics,
            a.d_type,
            a.create_time
            FROM
            $this->table a
            LEFT JOIN notes n ON n.note_id = a.note_id
            ORDER BY 
            a.create_time DESC
        ";
        //Prepare Statement
        $stmt = $this->conn->prepare($query);
        
        //Eecute Query
        $stmt->execute();

        return $stmt;
    }

    public function read_single(){

        $query = "
            SELECT
            a.id,
            a.d_content,
            a.d_resources,
            a.d_title,
            a.d_topics,
            a.d_type,
            a.note_id,
            a.create_time
            FROM
            $this->table a
            LEFT JOIN notes n ON n.note_id = a.id
            WHERE a.id = ?
            LIMIT 0,1
        ";

        //Prepare Statement
        $stmt = $this->conn->prepare($query);

        //Bind ID
        $stmt->bindParam(1,$this->id);

        //Execute Query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        //Set Properties
        $this->d_title      = $row['d_title'];
        $this->d_content    = $row['d_content'];
        $this->d_resources  = $row['d_resources'];
        $this->d_topics     = $row['d_topics'];
        $this->d_type       = $row['d_type'];
    }

    public function create(){
        //Insert Query
        $query = "INSERT INTO $this->table 
            SET
            d_title     = :d_title,
            d_content   = :d_content,
            d_resources = :d_resources,
            d_topics    = :d_topics,
            d_type      = :d_type
        ";

        //Prepare Statement
        $stmt = $this->conn->prepare($query);

        //Clean Data
        $this->d_title= htmlspecialchars(strip_tags($this->d_title));
        $this->d_content = htmlspecialchars(strip_tags($this->d_content));
        $this->d_resources = htmlspecialchars(strip_tags($this->d_resources));
        $this->d_topics = htmlspecialchars(strip_tags($this->d_topics));
        $this->d_type = htmlspecialchars(strip_tags($this->d_type));

        //Bind Data
        $stmt->bindParam(':d_title',$this->d_title);
        $stmt->bindParam(':d_content',$this->d_content);
        $stmt->bindParam(':d_resources',$this->d_resources);
        $stmt->bindParam(':d_topics',$this->d_topics);
        $stmt->bindParam(':d_type',$this->d_type);

        //Execute Query
        if($stmt->execute()){
            return true;
        }
        //Print Error if something goes wrong
        printf("Error:%s.\n",$stmt->error);
        return false;
    }

    public function update(){
        //Update Query
        $query = "UPDATE $this->table 
            SET
            d_title     = :d_title,
            d_content   = :d_content,
            d_resources = :d_resources,
            d_topics    = :d_topics,
            d_type      = :d_type
            WHERE id = :id
        ";

        //Prepare Statement
        $stmt = $this->conn->prepare($query);

        //Clean Data
        $this->d_title= htmlspecialchars(strip_tags($this->d_title));
        $this->d_content = htmlspecialchars(strip_tags($this->d_content));
        $this->d_resources = htmlspecialchars(strip_tags($this->d_resources));
        $this->d_topics = htmlspecialchars(strip_tags($this->d_topics));
        $this->d_type = htmlspecialchars(strip_tags($this->d_type));
        $this->id = htmlspecialchars(strip_tags($this->id));

        //Bind Data
        $stmt->bindParam(':d_title',$this->d_title);
        $stmt->bindParam(':d_content',$this->d_content);
        $stmt->bindParam(':d_resources',$this->d_resources);
        $stmt->bindParam(':d_topics',$this->d_topics);
        $stmt->bindParam(':d_type',$this->d_type);
        $stmt->bindParam(':id',$this->id);

        //Execute Query
        if($stmt->execute()){
            return true;
        }
        //Print Error if something goes wrong
        printf("Error:%s.\n",$stmt->error);
        return false;
    }

    public function delete(){
        //Delete Query
        $query = "
            DELETE FROM $this->table WHERE id = :id
        ";

        //Prepare Statement
        $stmt = $this->conn->prepare($query);

        //Clean Data
        $this->id= htmlspecialchars(strip_tags($this->id));   

        //Bind Data
        $stmt->bindParam(':id',$this->id);
        
        //Execute Query
        if($stmt->execute()){
            return true;
        }
        //Print Error if something goes wrong
        printf("Error:%s.\n",$stmt->error);
        return false;
    }
}
?>