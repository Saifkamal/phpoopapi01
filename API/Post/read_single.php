<?php
//Headers
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json');

include_once '../../Config/database.php';
include_once '../../Models/blog.php';

//Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

//Instantiate Blog Post Object
$post     = new Blog($db);

//Get ID
$post->id = isset($_GET['id']) ? $_GET['id'] : '';

//Get Post
$post->read_single();

//Create Array
$post_arr = array(
    'title'         => $post->d_title,
    'content'       => $post->d_content,
    'resources'     => $post->d_resources,
    'topics'        => $post->d_topics,
    'type'          => $post->d_type
);

//Make JSON
print_r(json_encode($post_arr));
?>