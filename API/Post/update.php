<?php
//Headers
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json');
header('Access-Control-Allow-Methods:PUT');
header('Access-Control-Allow-Headers:Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requesed-With');

include_once '../../Config/database.php';
include_once '../../Models/blog.php';

//Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

//Instantiate Blog Post Object
$post     = new Blog($db);

//Get the Raw Posted Data
$data     = json_decode(file_get_contents("php://input"));

//Set the ID to Update
$post->id = $data->id;

$post->d_title      = $data->d_title;
$post->d_content    = $data->d_content;
$post->d_resources  = $data->d_resources;
$post->d_topics     = $data->d_topics;
$post->d_type       = $data->d_type;

//Create Post
if ($post->update()) {
    echo json_encode(
        array(
            'message'   => 'Post Updated'
        )
    );
}else{
    echo json_encode(
        array(
            'message'   => 'Post Not Updated'
        )
    );
}
?>