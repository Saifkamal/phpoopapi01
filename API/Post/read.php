<?php
//Headers
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json');

include_once '../../Config/database.php';
include_once '../../Models/blog.php';

//Instantiate DB & Connect
$database = new Database();
$db = $database->connect();

//Instantiate Blog Post Object
$post     = new Blog($db);

//Blog Post Query
$result   = $post->read();

//Get Row Count
$num      = $result->rowCount();

//Check if any Posts
if($num > 0){
    //Post Array
    $posts_arr          = array();
    $posts_arr['data']  = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)){
        extract($row);

        $post_item = array(
            'create_time'   => $create_time,
            'd_abstract'    => $d_abstract,
            'd_content'     => $d_content,
            'd_keywords'    => $d_keywords,
            'd_resources'   => $d_resources,
            'd_title'       => $d_title,
            'd_topics'      => $d_topics,
            'd_type'        => $d_type,
            'note_id'       => $note_id
        );

        //Push to Data
        array_push($posts_arr['data'],$post_item);

        //Turn to JSON & output
        echo json_encode($posts_arr);
    }
}else{
    //No Posts/Blogs
    echo json_encode(
        array(
            'msg'   => 'No Data Found'
        )
    );
}

?>